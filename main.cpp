#include <iostream>
#include "game.h"
#include <windows.h>
using namespace std;

void draw(game& g)
{
    map& m=g.getMap();
    player* p=g.getPlayer();

    for(uint i=0;i<map::shape;i++)
    {
        for(uint j=0;j<map::shape;j++)
        {
            if(i==p->x && j==p->y)
                cout<<'8';
            else if(m.m[i][j]=='0' || m.m[i][j]=='x')
                cout<<' ';
            else if(m.m[i][j]=='1')
                cout<<'1';
            else
                cout<<m.m[i][j];
        }
        cout<<endl;
    }
    cout<<endl;
    cout<<"level:"<<p->getLevel()<<endl;
    cout<<"life:"<<p->getLife()<<endl;
}

moveResult control(game &g)
{
    uint op;
    cin>>op;
    if(op==6)
        return g.movePlayer(0,1);
    else if(op==4)
        return g.movePlayer(0,-1);
    else if(op==8)
        return g.movePlayer(-1);
    else if(op==2)
        return g.movePlayer(1);
    else
        return F;
}

void prompt(moveResult r)
{
    if(r==M)
        cout<<"Meet Cat!"<<endl;
    else if(r==X)
        cout<<"Meet Trap!"<<endl;
    else if(r==N)
        cout<<"Get Cheese!"<<endl;
    else if(r==F)
        cout<<"Move Failed!"<<endl;
    else
        cout<<"Move Success!"<<endl;
}

bool updateNotEnd(player* p)
{
    if(p->isDie())
        cout<<"You are Lose"<<endl;
    else if(p->isWin())
        cout<<"You are Win"<<endl;
    return !(p->isDie() || p->isWin());
}

int main()
{
    Rand::initRand();
    game g;
    bool notEnd=true;
    while(notEnd)
    {
        draw(g);
        moveResult r=control(g);
        prompt(r);
        notEnd=updateNotEnd(g.getPlayer());
        system("pause");
        system("cls");
    }
    return 0;
}
